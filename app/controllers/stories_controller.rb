class StoriesController < ApplicationController

  def index
    @stories = Story.all
    @categorystory = Categorystory.all

    @story = Story.all
    @personals = Personal.all
    @experience = Experience.all
    @education = Education.all
    @latestposts = Blog.limit(3).order("created_at ASC")
    @contact = Contact.all
  end

  def new
    @story = Story.new
  end

  def show
    @story = Story.find(params[:id])

    @categorystory = Categorystory.all
    @personals = Personal.all
    @experience = Experience.all
    @education = Education.all
    @latestposts = Blog.limit(3).order("created_at ASC")
    @contact = Contact.all
  end

  def create
    @story = Story.new(story_params)
    @story.save
  end

  private
    def story_params
      params.require(:story).permit(:title,:description,:post_date,:image,:categorystory_id,:author)
    end

end
