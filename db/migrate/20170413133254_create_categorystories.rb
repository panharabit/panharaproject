class CreateCategorystories < ActiveRecord::Migration[5.0]
  def change
    create_table :categorystories do |t|
      t.string :categoryname

      t.timestamps
    end
  end
end
