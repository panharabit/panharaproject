class CreateStories < ActiveRecord::Migration[5.0]
  def change
    create_table :stories do |t|
      t.string :title
      t.text :description
      t.date :post_date
      t.string :image
      t.references :categorystory, foreign_key: true

      t.timestamps
    end
  end
end
