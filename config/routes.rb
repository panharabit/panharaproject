Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  resources :categorystries do
    resources :stories
  end

  resources :stories
  
  resources :categoryskills do
    resources :skills
  end

  resources :eductions
  resources :personals
  resources :experiences
  resources :contacts
  resources :contactmes
  resources :blogs
  root "homes#index"
end
