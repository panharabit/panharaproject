ActiveAdmin.register Categorystory do
  permit_params :categoryname
  menu :parent => "Story"

  show :title => :categoryname

  form :html=> {:enctype=>"multipart/form-data"} do |f|
    f.inputs do
      f.input :categoryname
      actions
    end
  end
  
end
