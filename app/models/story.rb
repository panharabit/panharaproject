class Story < ApplicationRecord
  belongs_to :categorystory
  validates_presence_of :title,:description,:post_date,:image
  has_attached_file :image, styles: {
    medium: "300x300>",
    thumb: "100x100>"
  }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
